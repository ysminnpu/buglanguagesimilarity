%
% See LICENCE_BSD for licensing information
%
% Copyright Steven Davies 2012
%

\RequirePackage{atbegshi}
\documentclass[12pt]{beamer}

\usepackage{beamerframedblock}
\usepackage{calc}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{overpic}
\usepackage{spdmacros}
\usepackage{tikz}

\mode<presentation>
\definecolor{heading}{HTML}{660000}
\definecolor{border}{HTML}{996633}
\definecolor{footer}{HTML}{666633}
\definecolor{footerbg}{HTML}{CCCC99}

\setbeamertemplate{blocks}[framed]
\setbeamertemplate{block frame}{
  \pgfsetlinewidth{2pt}
  \pgfsetstrokecolor{border}
}

\setbeamersize{text margin left=0.5cm, text margin right=0.5cm}
\setbeamercolor{itemize item}{fg=border}
\setbeamercolor{itemize subitem}{fg=border}
\setbeamercolor{description item}{fg=border}
\setbeamertemplate{caption}{\insertcaption}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{%
  \begin{beamercolorbox}[right]{footlinebg} %
    \insertslidenavigationsymbol %
    \insertframenavigationsymbol %
    \insertsubsectionnavigationsymbol %
    \insertsectionnavigationsymbol %
    \insertframenumber/\inserttotalframenumber %
  \end{beamercolorbox} %
}
\setbeamertemplate{title page}{
\begin{tikzpicture}[remember picture, overlay]\node [xshift=-1cm,yshift=-1cm] at (current page.north east) { \includegraphics[height=2cm,width=2cm]{logo.png} }; \end{tikzpicture}
\centering
\\ \vspace*{\vfill}
\begin{block}{}
\centering
\inserttitle
\end{block}
\\ \vspace*{\vfill}
\insertauthor
\\
\insertinstitute
\\ \vspace*{\vfill}
\insertdate
\\ \vspace*{\vfill}
}

\newlength{\narrowitemsep}
\setlength{\narrowitemsep}{\itemsep}
\newlength{\wideitemsep}
\setlength{\wideitemsep}{\itemsep}
\addtolength{\wideitemsep}{0.5\baselineskip}
\let\olditem\item
\let\subitem\item
\renewcommand{\item}{\setlength{\itemsep}{\wideitemsep}\olditem}

\setbeamertemplate{frametitle}{\begin{block}{}\large\strut\centering\insertframetitle\end{block}}
\setbeamercolor{titlelike}{fg=heading,bg=white}
\setbeamercolor{block body}{fg=heading,bg=white}
\setbeamercolor{block title example}{fg=white,bg=border}
\setbeamercolor{block body example}{fg=black,bg=footerbg}
\setbeamercolor{footline}{fg=footer,bg=heading}
\setbeamercolor{footlinebg}{fg=footer,bg=footerbg}
\setbeamerfont*{itemize/enumerate subbody}{parent=itemize/enumerate body}
\setbeamerfont*{itemize/enumerate subsubbody}{parent=itemize/enumerate body}


\newcommand{\drawdocument}[3]{ 
    \only<6-> { \draw (#1,#2) ++(0.5, 1.25) node {#3}; } 
    \drawbox{#1}{#2}
    \draw[thick] (#1,#2) ++(0.1,0.7) -- ++(0.6,0); 
    \draw[thick] (#1,#2) ++(0.1,0.6) -- ++(0.3,0); 
    \draw[thick] (#1,#2) ++(0.5,0.6) -- ++(0.3,0); 
    \draw[thick] (#1,#2) ++(0.1,0.5) -- ++(0.7,0); 
    \draw[thick] (#1,#2) ++(0.1,0.4) -- ++(0.8,0); 
    \draw[thick] (#1,#2) ++(0.1,0.3) -- ++(0.4,0); 
    \draw[thick] (#1,#2) ++(0.6,0.3) -- ++(0.2,0); 
    \draw[thick] (#1,#2) ++(0.1,0.2) -- ++(0.2,0); 
    \draw[thick] (#1,#2) ++(0.3,0.2) -- ++(0.5,0); 
    \draw[thick] (#1,#2) ++(0.1,0.1) -- ++(0.8,0); 
}

\newcommand{\drawbox}[2]{ 
    \draw[rounded corners,fill=footerbg] (#1,#2) -- ++(0,1) -- ++(1,0) -- ++(0,-1) -- cycle; 
    \draw[fill=border] (#1,#2) ++(0,1) -- ++(0,-0.2) -- ++(1,0) [rounded corners] -- ++(0,0.2) -- cycle; 
    \draw[thick] (#1,#2) ++(0.1,0.9) -- ++(0.8,0); 
}
\newcommand{\drawclassifiera}[2]{
    \only<6,7> { \draw (#1,#2) ++(0.5, 1.25) node {N}; } 
    \drawbox{#1}{#2}
    \draw[ultra thick] (#1,#2) ++(0.5,0.1) -- ++(0,0.6); 
}
\newcommand{\drawclassifierb}[2]{
    \only<6,7> { \draw (#1,#2) ++(0.5, 1.25) node {Y}; } 
    \drawbox{#1}{#2}
    \draw[ultra thick] (#1,#2) ++(0.1,0.1) -- ++(0.1,0.3) -- ++(0.3,0.3) -- ++(0.3,-0.3) -- ++(0.1,-0.3);
}
\newcommand{\drawclassifierc}[2]{
    \only<6,7> { \draw (#1,#2) ++(0.5, 1.25) node {N}; } 
    \drawbox{#1}{#2}
    \draw[ultra thick] (#1,#2) ++(0.1,0.1) -- ++(0.1,0.3) -- ++(0.3,0.3) -- ++(0.3,-0.3) -- ++(0.1,-0.3);
    \draw[ultra thick] (#1,#2) ++(0.6,0.1) -- ++(0.2,0.3) -- ++(0.1,-0.3);
}
\newcommand{\drawclassifierd}[2]{
    \only<6,7> { \draw (#1,#2) ++(0.5, 1.25) node {N}; } 
    \drawbox{#1}{#2}
    \draw[ultra thick] (#1,#2) ++(0.1,0.1) -- ++(0.1,0.3) -- ++(0.3,0.3) -- ++(0.3,-0.3) -- ++(0.1,-0.3);
    \draw[ultra thick] (#1,#2) ++(0.6,0.1) -- ++(0.2,0.3) -- ++(0.1,-0.3);
    \draw[ultra thick] (#1,#2) ++(0.1,0.1) -- ++(0.1,0.3) -- ++(0.2,-0.3);
}

\newcommand{\drawbigtreea}{ 
    \draw[ultra thick] (0,0) ++(3,0) node[below] {N} -- ++(0,1); 
    \useasboundingbox (0,0) rectangle (6,1);
}
\newcommand{\drawbigtreeb}{
    \draw[ultra thick] (0,0) node[below] {Y} -- ++(1,0.5) -- ++(2,0.5) -- ++(2,-0.5) -- ++(1,-0.5) node[below] {N};
}
\newcommand{\drawbigtreec}{
    \draw[ultra thick] (0,0) node[below] {Y} -- ++(1,0.5) -- ++(2,0.5) -- ++(2,-0.5) -- ++(1,-0.5) node[below] {N};
    \draw[ultra thick] (0,0) ++(4,0) node[below] {Y} -- ++(1,0.5) -- ++(1,-0.5);
}
\newcommand{\drawbigtreed}{
    \draw[ultra thick] (0,0) node[below] {Y} -- ++(1,0.5) -- ++(2,0.5) -- ++(2,-0.5) -- ++(1,-0.5) node[below] {N};
    \draw[ultra thick] (0,0) ++(4,0) node[below] {Y} -- ++(1,0.5) -- ++(1,-0.5);
    \draw[ultra thick] (0,0) -- ++(1,0.5) -- ++(1,-0.5) node[below] {N};
}
\newcommand<>{\lineitem}[1]{\onslide#2 \begin{itemize} \item #1 \end{itemize}}

\newcommand{\picunder}[2]{\begin{overpic}[width=0.9\textwidth,tics=10]{#1}{#2}\end{overpic}}
\newcommand<>{\textover}[3]{%
    \only#4{%
        \put (#1,#2){%
            \begin{beamerboxesrounded}[width=0.65\textwidth,lower=block body example,shadow=true]{}%
                \begin{flushleft}\small #3\end{flushleft}%
            \end{beamerboxesrounded}%
        }%
    }%
}

\author{Steven Davies \and Marc Roper \and Murray Wood}
\institute{
Department of Computer and Information Sciences \\
University of Strathclyde
}
\date[WCRE '12]{Working Conference on Reverse Engineering, 2012}

\SweaveOpts{echo=false,eps=false,prefix.string=tmp/figure}
<<echo=FALSE>>=
    options(device= function(...) { .Call('R_GD_nullDevice', PACKAGE = 'grDevices') })
@

\SweaveInput{../common.Rnw}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section{Introduction}
% 1 minute

\begin{frame}{Introduction}
\begin{itemize}
    \item We improved the performance of bug localisation techniques by mining information from previously fixed similar bug reports
    \item Given a bug report, bug localisation tools present developers with recommendations for where the bug is likely to be located
    \item Developers can spend up to 50\% of their time on bug-fixing and maintenance, and often a large part is locating where the bug is
\end{itemize}
\end{frame}

\section{Existing Techniques}
% 3 minutes

\begin{frame}{Bug Localisation}
    \picunder{report.png}{
        \textover<2>{30}{-10}{
            I just updated to pre 10 from pre 9 and my perl Syntax coloring is now messed up. It seems that escaped double quotes within double quoted strings are messing up my matches (and they weren't before). This is causing all kinds of colorization issues. How can I correct this (which file do I look at). and is there a simple way to put the perl mode back to pre9 and keep pre 10 installed? I have looked for the file and not had any luck. Thanks 
        }
        \textover<3>{30}{-20}{
            ParserRule.ParserRule() \\
            ParserRule.createEOLSpanRule() \\
            ParserRule.createEscapeRule() \\
            ParserRule.createMarkFollowingRule() \\
            ParserRule.createMarkPreviousRule() \\
            ParserRule.createRegexpEOLSpanRule() \\
            ParserRule.createRegexpSequenceRule() \\
            ParserRule.createRegexpSpanRule() \\
            ParserRule.createSequenceRule() \\
            ParserRule.createSpanRule() \\
            ParserRule.toString() \\
            ParserRuleSet.setEscapeRule() \\
            TokenMarker.LineContext.LineContext() \\
            ...
        }
    }
\end{frame}

\begin{frame}{Bug Localisation}
\begin{itemize}
    \item Existing bug localisation techniques are somewhat effective
    \item Still not widely used
    \item Often the first relevant method shown is vastly outwith those a developer would realistically examine
    \item There are a number of sources of additional information not currently utilised
    \item This work focuses on improvements to textual / information retrieval-based techniques
\end{itemize}
\end{frame}

\begin{frame}{Source Code Approach}
\begin{overprint}
    \lineitem<1>{First, each method in the system is extracted, including comments}
    \lineitem<2>{Terms are split up on changes in case or punctuation, and common stop-words are removed}
    \lineitem<3>{This is done for every method in the system}
    \lineitem<4>{When a bug is reported we extract the description}
    \lineitem<5>{The description is pre-processed in the same way and submitted as a query}
    \lineitem<6>{The similarity of each method is calculated (Currently we use cosine similarity and TF-IDF)}
    \lineitem<7>{The methods are presented to the developer in order}
\end{overprint}
\only<1>{
    \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{public LineContext markTokens(LineContext,TokenHandler,Segment)}
        \lstinputlisting[language=Java,breaklines=true,breakatwhitespace=true,emptylines=0]{example}
    \end{beamerboxesrounded}
}
\only<2->{
    \begin{overprint}
        \onslide<2>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{TokenMarker.markTokens(LineContext,TokenHandler,Segment)}
                around avoid context count handler having instance last length line lots new offset parameters pass set token up variables ...
            \end{beamerboxesrounded}
        \onslide<4>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{Bug 1659666}
                \lstinputlisting[breaklines=true,breakatwhitespace=true,emptylines=0]{bug}
            \end{beamerboxesrounded}
        \onslide<5,6>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{Bug 1659666}
                accepted brackets curly documented handle html http like only operators org patch perl perldoc perlop quote round section slash without xml ...
            \end{beamerboxesrounded}
        \onslide<7>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{Bug 1659666}
                0.9 XmlParser.setHandler(XmlHandler) \newline
                0.7 jEdit.getInputHandler() \newline
                0.6 TokenMarker.markTokens(LineContext,TokenHandler,Segment \newline
                ...
            \end{beamerboxesrounded}
    \end{overprint}
    \begin{overprint}
        \onslide<3->
            \begin{tikzpicture}
                \tikzset{hidden/.style={draw opacity=0}}
                \tikzset{reversed/.style={->}}
                \only<5->{
                    \tikzset{hidden/.style={draw opacity=1}}
                }
                \only<7>{
                    \tikzset{reversed/.style={<-}}
                }
                \draw[ultra thick,hidden,reversed] (5,2) -- (5,1.5);
                \drawdocument{0}{0}{0.7}
                \drawdocument{2}{0}{0.9}
                \drawdocument{4}{0}{0.3}
                \drawdocument{6}{0}{0.6}
                \drawdocument{8}{0}{0.1}
                \draw[fill=black] (10,0.5) circle [radius=0.1];
                \draw[fill=black] (10.3,0.5) circle [radius=0.1];
                \draw[fill=black] (10.6,0.5) circle [radius=0.1];
            \end{tikzpicture}
    \end{overprint}
}
\end{frame}

\begin{frame}{Shortcomings}
\begin{itemize}
    \item Three bugs raised in jEdit
        \begin{itemize}
            \subitem 1659666 --- perl.xml patch for quote-like operators
            \subitem 1760646 --- perl colorization 
            \subitem 1807549 --- Perl escape still not working correctly 
        \end{itemize}
    \item Bug descriptions are fairly similar: all are related to Perl, and syntax in particular
    \item All required a change to \code{org\br{.}gjt\br{.}sp\br{.}jedit\br{.}syntax\br{.}TokenMarker\br{.}markTokens(LineContext, TokenHandler, Segment)}
    \item Method makes no mention of \emph{Perl} and textual approach would likely not identify these methods
\end{itemize}
\end{frame}

\section{Our Approach}
% 4 minutes

\begin{frame}{Bug Description Approach}
\begin{itemize}
    \item Seek to overcome these problems by taking into consideration past fixed bugs
    \item For each method in the system we train a classifier to answer a simple question:
        \begin{itemize}
            \item Is this new bug related to this method?
        \end{itemize}
    \item Training is based on the text of the bugs related to the method, \emph{not} the text of the method itself
    \item Classifiers are updated whenever new bugs are fixed, identified through commit comments
\end{itemize}
\end{frame}

\begin{frame}{Bug Description Approach}
\begin{overprint}
    \lineitem<1>{Each method is represented by an initially empty classifier}
    \lineitem<2>{When bugs fixed, relevant methods updated with description}
    \lineitem<3>{The classifiers grow as more bugs are fixed}
    \lineitem<4>{Classifiers are also updated with which bugs were \emph{not} related}
    \lineitem<5>{When bugs are reported description is submitted as a query}
    \lineitem<6>{Each classifier predicts either yes or no}
    \lineitem<7>{Predicted methods are presented to the developer (unordered)}
\end{overprint}
\begin{overprint}
    \begin{overprint}
        \onslide<2>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{Bug 1292706}
                abnormal adjustments appears attachment basically behaviour calculated component correctly determined dialog display displayed ...
            \end{beamerboxesrounded}
        \onslide<3>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{Bug 1382388}
                after append appendrows autosizecolumn before bug causes code command ctrl data else example filename found getnumberrows ...
            \end{beamerboxesrounded}
        \onslide<4>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{Bug 1818140}
            appears can changed class click clicking default list model delete double edit entered entries history text insert limited list model ...
            \end{beamerboxesrounded}
        \onslide<5,6>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{Bug 1659666}
                accepted brackets curly documented handle html http like only operators org patch perl perldoc perlop quote round section slash ...
            \end{beamerboxesrounded}
        \onslide<7>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{Bug 1659666}
                Y XmlParser.setHandler(XmlHandler) \newline
                Y jEdit.getInputHandler() \newline
                Y TokenMarker.markTokens(LineContext,TokenHandler,Segment \newline
            \end{beamerboxesrounded}
    \end{overprint}
    \begin{overprint}
        \onslide<1-4>
            \begin{beamerboxesrounded}[lower=block body example,upper=block title example]{TokenMarker.markTokens(LineContext,TokenHandler,Segment)}
                \centering
                \begin{tikzpicture}
                    \only<1>{ \drawbigtreea }
                    \only<2>{ \drawbigtreeb }
                    \only<3>{ \drawbigtreec }
                    \only<4>{ \drawbigtreed }
                \end{tikzpicture}
            \end{beamerboxesrounded}
    \end{overprint}
\end{overprint}
\begin{tikzpicture}
    \only<5,6>{
        \draw[ultra thick,->] (5,2) -- (5,1.5);
    }
    \only<7>{
        \draw[ultra thick,<-] (5,2) -- (5,1.5);
    }
    \only<1>{ \drawclassifiera{0}{0} }
    \only<2->{ \drawclassifierb{0}{0} }
    \only<1->{ \drawclassifiera{2}{0} }
    \only<1>{ \drawclassifiera{4}{0} }
    \only<2>{ \drawclassifierb{4}{0} }
    \only<3->{ \drawclassifierc{4}{0} }
    \only<1>{ \drawclassifiera{6}{0} }
    \only<2>{ \drawclassifierb{6}{0} }
    \only<3>{ \drawclassifierc{6}{0} }
    \only<4->{ \drawclassifierd{6}{0} }
    \only<1,2>{ \drawclassifiera{8}{0} }
    \only<3->{ \drawclassifierb{8}{0} }
    \draw[fill=black] (10,0.5) circle [radius=0.1];
    \draw[fill=black] (10.3,0.5) circle [radius=0.1];
    \draw[fill=black] (10.6,0.5) circle [radius=0.1];
\end{tikzpicture}
\end{frame}

\begin{frame}{Bug Description Approach}
\begin{itemize}
    \item Current classifiers are simple C4.5 decision trees
    \item Only useful for methods which have been involved in multiple bugs
        \begin{itemize}
            \item At least one previous bug needed to train classifier
        \end{itemize}
    \item Lack of order is an issue
    \item Can be overcome by combining the two approaches
\end{itemize}
\end{frame}

\begin{frame}{Combining Results}
\begin{overprint}
    \lineitem<1>{Source Code Approach used to sort all methods}
    \lineitem<2>{Bug Description Approach used to partition all methods into either relevant or not relevant}
    \lineitem<3>{Relevant methods are then promoted to the top of the list, still sorted by source code technique}
\end{overprint}
\begin{overprint}
    {
        \footnotesize 
        \onslide<1> 
            \def\svgwidth{0.25\textwidth}
            \centerline{\input{partition1.pdf_tex}}
        \onslide<2> 
            \def\svgwidth{0.25\textwidth}
            \centerline{\input{partition2.pdf_tex}}
        \onslide<3> 
            \def\svgwidth{0.25\textwidth}
            \centerline{\input{partition3.pdf_tex}}
    }
\end{overprint}
\end{frame}

\section{Evaluation}
% 1 minute

\begin{frame}{Evaluation}
<<echo=FALSE>>=
    strip_revision <- function(x) sapply(strsplit(as.character(x), ' (', fixed=TRUE), `[`, 1)
    projects$`Start Version` <- strip_revision(projects$`Start Version`)
    projects$`End Version` <- strip_revision(projects$`End Version`)
    project_names <- paste(rownames(projects), ' v', projects$`Start Version`, '--v', projects$`End Version`, sep='', collapse=', ')
    num_bugs_per_method <- rbind(argouml$bugs_per_method, jabref$bugs_per_method, jedit$bugs_per_method, mucommander$bugs_per_method)
    applicable_methods <- num_bugs_per_method[3]
@
    \begin{itemize}
        \item Evaluated on four projects: \Sexpr{project_names}
        \item During this time, \Sexpr{min(projects$`Evaluated Bugs`)}--\Sexpr{max(projects$`Evaluated Bugs`)} bugs were raised for which the fixing changes could be identified (Total: \Sexpr{sum(projects$`Evaluated Bugs`)} bugs)
        \item Projects contained \Sexpr{min(projects$`Methods`)}--\Sexpr{max(projects$`Methods`)} methods
        \item \Sexpr{sum(applicable_methods)} methods were involved in more than one bug fix, related to \Sexpr{sum(projects$`Applicable Bugs`)} of the bugs
        \item Measured the position of the first method presented to users which was actually involved in the fixing of the bug
    \end{itemize}
\end{frame}

\section{Results}
% 4 minutes

\begin{frame}{ArgoUML Bug 4364}
    \begin{itemize}
        \item Summary: ``Dragging association onto diagram creates n-ary diamond''
        \item Required changes to~\Sexpr{argouml_bug_4364$`Methods updated`} methods
        \item First returned by Source Code Approach was \code{\Sexpr{escape(argouml_bug_4364$`FRM (Direct)`)}} at position~\Sexpr{argouml_bug_4364$`FRM Rank (Direct)`} (out of \Sexpr{argouml$stats$num_methods})
        \item Bug Description Approach predicted~\Sexpr{argouml_bug_4364$`Predicted methods`} methods to be relevant 
        \item When combined, this method rose to position \Sexpr{argouml_bug_4364$`FRM Rank (Combined)`}
    \end{itemize}
\end{frame}

\begin{frame}{JabRef Bug 1540646}
    \begin{itemize}
        \item First relevant method returned was originally \code{\Sexpr{escape(jabref_bug_1540646$`FRM (Direct)`)}}, at position \Sexpr{jabref_bug_1540646$`FRM Rank (Direct)`}
        \item None of the \Sexpr{jabref_bug_1540646$`Predicted methods`} methods predicted were relevant
        \item First relevant method fell to position \Sexpr{jabref_bug_1540646$`FRM Rank (Combined)`}
    \end{itemize}
\end{frame}
        
<<echo=FALSE>>=
    changes<-c(argouml$improvement, jabref$improvement, jedit$improvement, mucommander$improvement)
    improvements<-abs(changes[changes>0])
    decreases<-abs(changes[changes<0])
    frm<-c(argouml$frm, jabref$frm, jedit$frm, mucommander$frm)
    cmb_frm<-c(argouml$cmb_frm, jabref$cmb_frm, jedit$cmb_frm, mucommander$cmb_frm)
    improved<-cmb_frm-frm<0
    worsened<-cmb_frm-frm>0

    colours1<-c('#FFFFB2', '#FECC5C', '#FD8D3C', '#F03B20', '#BD0026')
    colours2<-c('#FFFFCC', '#A1DAB4', '#41B6C4', '#2C7FB8', '#253494')
@
\begin{frame}{Results}
\begin{overprint}
    \lineitem<1>{In total \Sexpr{sum(improved)} bugs improved their position, by between \Sexpr{min(improvements)} and \Sexpr{max(improvements)} places}
    \lineitem<2>{\Sexpr{sum(worsened)} bugs got worse, but the amount they did so by was smaller, only \Sexpr{min(decreases)}--\Sexpr{max(decreases)} places}
\end{overprint}
\begin{overprint}
    \onslide<1>
<<fig=TRUE, height=5>>=
    par(mai=c(1.25, 0.75, 0.25, 0.5),las=1)
    plot(sort(frm[improved]), type='l', lty=1, col=colours1[5], lwd=3, log='y', ylim=c(1,10000), bty='l', xlab='', ylab='FRM')
    lines(sort(cmb_frm[improved]), lty=5, col=colours2[5], lwd=3)
    legend('top', c('Source code', 'Combined'), lty=c(1,5), col=c(colours1[5], colours2[5]), lwd=3, seg.len=4)
@
    \onslide<2>
<<fig=TRUE, height=5>>=
    par(mai=c(1.25, 0.75, 0.25, 0.5),las=1)
    plot(sort(frm[worsened]), type='l', lty=1, col=colours1[5], lwd=3, log='y', ylim=c(1,10000), bty='l', xlab='', ylab='FRM')
    lines(sort(cmb_frm[worsened]), lty=5, col=colours2[5], lwd=3)
    legend('top', c('Source code', 'Combined'), lty=c(1,5), col=c(colours1[5], colours2[5]), lwd=3, seg.len=4)
@
\end{overprint}
\end{frame}

\begin{frame}{Results}
\begin{overprint}
    \lineitem<1>{Overall the number of bugs where the first relevant method is at position 1 increased from \Sexpr{top1} to \Sexpr{cmb_top1}}
    \lineitem<2>{Bugs in top-10 increased from \Sexpr{top10} to \Sexpr{cmb_top10}}
\end{overprint}
<<fig=TRUE, height=5>>=
par(mai=c(1.25, 0.75, 0.25, 0.5),las=1)
calculate_groups <- function(x) { 
    ret <- x - rbind(0, x[-nrow(x),]) 
    ret[c(nrow(ret):1),]
}
x<-calculate_groups(breaks)
y<-calculate_groups(cmb_breaks)
gap<-1
big_gap<-2.5
spaces<-rep(1+gap+big_gap, ncol(x)-1)
barplot(x, space=c(gap,spaces), col=colours1, ylim=c(0,200))
barplot(y, space=c(1+2*gap,spaces), col=colours2, add=TRUE, axisnames=FALSE)
for(i in c(1:ncol(breaks))){
    x1 <- 2*i-1 + i*gap + (i-1)*big_gap
    x2 <- x1 + gap
    total1 <- breaks[nrow(breaks),i]
    total2 <- breaks[nrow(cmb_breaks),i]
    lines(c(x1,x2), c(total1, total2), lty=2)
    for(j in c(1:nrow(breaks))){
        y1 <- total1 - breaks[j,i]
        y2 <- total2 - cmb_breaks[j,i]
        lines(c(x1,x2), c(y1, y2), lty=2)
    }
}
text(2.5, 180, 'Source code')
text(8, 180, 'Combined')
legend(2.5, 180, c('Top-1', 'Top-10', 'Top-100', 'Top-1000', '>1000'), fill=rev(colours1), bty='n')
legend(7, 180, c('', '', '', '', ''), fill=rev(colours2), bty='n')
           
@
\end{frame}

\begin{frame}{Results}
\begin{itemize}
    \item Also measured change in mean reciprocal rank (the inverse of the position of the first relevant method)
    \item Mean reciprocal rank rose for all 4 projects
\end{itemize}
<<results=tex>>=
    test_rr <- function(project){
        rr <- 1/project$frm
        cmb <- 1/project$cmb_frm
        c(mean(rr), mean(cmb))
    }
    rr<-sapply(list(argouml, jabref, jedit, mucommander), test_rr)
    colnames(rr)<-rownames(projects)
    rownames(rr)<-c('Source Code', 'Combined')
    xtable(t(rr), digits=4)
@
\end{frame}
        
\begin{frame}{Results}
\begin{itemize}
    \item Change in position was modest, but simple to achieve
    \item Our overall aim is to combine multiple sources of additional information
        \begin{itemize}
            \subitem Methods involved in similar bugs
            \subitem Recently changed methods
            \subitem Untested methods
            \subitem Methods in stack traces
        \end{itemize}
    \item Each likely to provide only small improvement, but combined effect should be larger
\end{itemize}
\end{frame}

\section{Summary}
% 1 minute

\begin{frame}{Summary}
\begin{itemize}
    \item Used textual information to identify similar bug reports, and used the location of previous fixes to improve bug localisation
    \item Evaluated \Sexpr{num_bugs} bugs from \Sexpr{num_projects} projects
    \item Improved mean reciprocal rank for all projects
    \item Increased the number of bugs where the first relevant method is at position 1 from \Sexpr{top1} to \Sexpr{cmb_top1}
\end{itemize}
\begin{center}
    \begin{minipage}{30ex}
        \begin{center}
            \begin{block}{}\begin{center}Thank You\end{center}\end{block}
            Steven.Davies@cis.strath.ac.uk
        \end{center}
    \end{minipage}
\end{center}
\end{frame}

\end{document}
