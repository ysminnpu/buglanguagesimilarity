#!groovy
/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies 2012
 */

property(file: 'build.properties')

java_dir = '../java/'
java_src_dir = "${java_dir}/src/"
java_build_dir = "${java_dir}/build/"
python_dir = '../python/src'
report_dir = '../latex/paper'
presentation_dir = '../latex/presentation'
property(name:'sweave_cache_dir', location:"../latex/cache")

path(id: 'classpath'){
	fileset(dir: java_lib_dir){
		include(name:'**/commons-io-2.2.jar')
		include(name:'**/commons-lang*.jar')
		include(name:'**/guava-*.jar')
		include(name:'**/org.eclipse.core.contenttype*.jar')
		include(name:'**/org.eclipse.core.jobs*.jar')
		include(name:'**/org.eclipse.core.resources*.jar')
		include(name:'**/org.eclipse.core.runtime*.jar')
		include(name:'**/org.eclipse.equinox.common*.jar')
		include(name:'**/org.eclipse.equinox.preferences*.jar')
		include(name:'**/org.eclipse.jdt.core*.jar')
		include(name:'**/org.eclipse.osgi*.jar')
		include(name:'**/org.eclipse.text*.jar')
                include(name:'**/weka*.jar')
	}
}

path(id: 'run_classpath'){
	path(refid: 'classpath')
	pathelement(path: java_build_dir)
}

path(id: 'taskspath'){
	path(refid: 'classpath')
        fileset(dir: java_lib_dir){
            include(name:'**/ant_latex_*.jar')
        }
}

path(id: 'pythonpath'){
	dirset(dir: python_lib_dir){
		include(name:'Library')
		include(name:'gensim-*')
		include(name:'scipy*')
		include(name:'numpy*')
	}
}
ant.property(name:"pythonpath", refid:"pythonpath")

def debug = {
    project.each { k, v ->
        println(k + ': ' + v)
    }
}

def download = {
        delete(dir: project['working_copy'])
        if( project['repository_type'] == 'git' ){
            def args = ['git', 'clone', project['repository'], project['working_copy']]
            println(args)
            args.execute().in.eachLine { println(it) }

            exec(dir: project['working_copy'], executable: 'git'){
                arg(value: 'checkout')
                arg(value: project['start_revision'])
            }
        }
        else {
            def args = ['svn', 'co', '-r', project['start_revision'], project['repository'], project['working_copy']]
            if( project['svn_auth'] ){
                args[1..<1] = Arrays.asList(project['svn_auth'].split())
            }
            println(args)
            args.execute().in.eachLine { println(it) }
        }
}

def links = {
	depends(compile)
	ant.java(classname: 'uk.ac.strath.cis.spd.buglanguage.links.BugMethodFinder', classpathref: 'run_classpath', failonerror: true, fork: true, maxmemory:'1024M'){
		arg(file: project['config'])
	}
}

def bugs = {
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'DownloadBugData.py')
		arg(file: project['config'])
	}
}

def parse = {
	depends(compile)
	ant.java(classname: 'uk.ac.strath.cis.spd.buglanguage.parse.JavaParser', classpathref: 'run_classpath', failonerror: true, fork: true, maxmemory:'1024M'){
		arg(file: project['config'])
	}
}

def relevant = {
        depends(clean_cache)
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'ListRelevantMethods.py')
		arg(file: project['config'])
	}
}

def stats = {
        depends(clean_cache)
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'Stats.py')
		arg(file: project['config'])
	}
}

def aggregate = {
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'Aggregator.py')
		arg(file: project['config'])
	}
}

def classify = {
        depends(clean_cache)
	depends(compile)
	ant.java(classname: 'uk.ac.strath.cis.spd.buglanguage.evaluate.FrmEvaluation', classpathref: 'run_classpath', failonerror: true, fork: true, maxmemory:'1024M'){
		arg(file: project['config'])
	}
}

def bug_count = {
        depends(clean_cache)
	depends(compile)
	ant.java(classname: 'uk.ac.strath.cis.spd.buglanguage.evaluate.BugCountEvaluation', classpathref: 'run_classpath', failonerror: true, fork: true, maxmemory:'1024M'){
		arg(file: project['config'])
	}
}

def create_corpus = {
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'CorporaCreator.py')
		arg(file: project['config'])
	}
}

def evaluate = {
        depends(clean_cache)
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'CorpusEvaluator.py')
		arg(file: project['config'])
	}
}

def combine = {
        depends(clean_cache)
	depends(compile)
	ant.java(classname: 'uk.ac.strath.cis.spd.buglanguage.evaluate.CombineEvaluation', classpathref: 'run_classpath', failonerror: true, fork: true, maxmemory:'1024M'){
		arg(file: project['config'])
	}
}

def frm = {
        depends(clean_cache)
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'FindFrm.py')
		arg(file: project['config'])
	}
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'AssessMethods.py')
		arg(file: project['config'])
	}
	ant.exec(executable:'python', dir: python_dir, failonerror: true){
		env(key:'PYTHONPATH', path:pythonpath)
		arg(value:'CountPredictions.py')
		arg(file: project['config'])
	}
}

target(compile:'Compiles Java classes'){
	mkdir(dir:java_build_dir)
	javac(srcdir:java_src_dir, destdir: java_build_dir, classpathref:'classpath', debug:true) {
		include(name: '**/*.java')
	}
}

target(clean_cache: 'Cleans Sweave cache whenever new reports are generated'){
    delete(dir:sweave_cache_dir)
    mkdir(dir:sweave_cache_dir)
}

target(report:'Generates final report from evaluation files'){
        ant.taskdef(name:'latex', classname:'de.dokutransdata.antlatex.LaTeX', classpathref:'taskspath')

        tmp_dir = "${report_dir}/tmp"
        delete(dir:tmp_dir)
        delete(file:"${report_dir}/paper.tex")
        delete(file:"${report_dir}/paper.pdf")

        mkdir(dir:tmp_dir)
        mkdir(dir:sweave_cache_dir)
        exec(executable:'Sweave.sh', dir:report_dir, failonerror:true){
            env(key:'R_LOCAL_LIB', value:r_lib_dir)
            env(key:'R_RESULTS', value:results_dir)
            arg(value:"--cache=${sweave_cache_dir}")
            arg(value:'paper.Rnw')
        }
        latex(workingDir:report_dir, pdftex:'on', verbose:'on', clean:'off'){
            bibtex(run:'on', workingDir:report_dir)
            fileset(dir:report_dir){
                include(name:'paper.tex')
            }
        }
        latex(workingDir:report_dir, pdftex:'on', verbose:'on', clean:'off'){
            bibtex(run:'on', workingDir:report_dir)
            fileset(dir:report_dir){
                include(name:'paper.tex')
            }
        }
        exec(executable:'pdftops', dir:report_dir, failonerror:true){
            arg(value:'paper.pdf')
            arg(value:'paper.ps')
        }
        exec(executable:'ps2pdf14', dir:report_dir, failonerror:true){
            arg(value:'-dPDFSETTINGS=/prepress')
            arg(value:'paper.ps')
            arg(value:'paper.pdf')
        }
        delete(file:"${report_dir}/paper.ps")
}

target(presentation:'Generates final presentation from evaluation files'){
        ant.taskdef(name:'latex', classname:'de.dokutransdata.antlatex.LaTeX', classpathref:'taskspath')

        tmp_dir = "${presentation_dir}/tmp"
        delete(dir:tmp_dir)
        delete(file:"${presentation_dir}/presentation.tex")
        delete(file:"${presentation_dir}/presentation.pdf")

        mkdir(dir:tmp_dir)
        mkdir(dir:sweave_cache_dir)
        exec(executable:'Sweave.sh', dir:presentation_dir, failonerror:true){
            env(key:'R_LOCAL_LIB', value:r_lib_dir)
            env(key:'R_RESULTS', value:results_dir)
            arg(value:"--cache=${sweave_cache_dir}")
            arg(value:'presentation.Rnw')
        }
        latex(workingDir:presentation_dir, pdftex:'on', verbose:'on', clean:'off'){
            bibtex(run:'on', workingDir:presentation_dir)
            fileset(dir:presentation_dir){
                include(name:'presentation.tex')
            }
        }
        latex(workingDir:presentation_dir, pdftex:'on', verbose:'on', clean:'off'){
            bibtex(run:'on', workingDir:presentation_dir)
            fileset(dir:presentation_dir){
                include(name:'presentation.tex')
            }
        }
}

def targets = [
    [ name: 'debug', description: 'Prints info for', depends: debug ],
    [ name: 'download', description: 'Downloads', depends: download, exclude: true ],
    [ name: 'links', description: 'Find old bug -> method links for', depends: links, exclude: true ],
    [ name: 'bugs', description: 'Downloads old bug descriptions for', depends: bugs, exclude: true ],
    [ name: 'parse', description: 'Parses working copy of', depends: parse, exclude: true ],
    [ name: 'relevant', description: 'Parses gold set for use by other tasks', depends: relevant ],
    [ name: 'aggregate', description: 'Aggregates project from lower granularity levels', depends: aggregate ],
    [ name: 'stats', description: 'Calculates stats for', depends: stats ],
    [ name: 'model', description: 'Creates language model and index for', depends: create_corpus ],
    [ name: 'evaluate', description: 'Evaluates performance for', depends: evaluate ],
    [ name: 'classify', description: 'Creates and evaluates bug language similarity classifier for', depends: classify ],
    [ name: 'bug_count', description: 'Creates and evaluates bug count classifier for', depends: bug_count ],
    [ name: 'combine', description: 'Combines results for', depends: combine ],
    [ name: 'frm', description: 'Finds first relevant methods for all evaluations', depends: frm ],
]

def projects = []
new File('.').eachFile { file ->
	if(file.name.endsWith('.config')){ 
		def project_name = file.name.replace('.config', '')
		projects << project_name
		target( (project_name): 'Builds ' + project_name ) {
			project = new Properties()
			new File(file.name).withInputStream { project.load(it) }
			project['config'] = file.name
		}
                targets.each { t ->
                    target( (t['name'] + '_' + project_name): (t['description'] + ' ' + project_name) ) {
                        depends(project_name)
                        (t['depends'])()
                    }
                }
	}
}                                    

targets.each { t ->
    target( (t['name'] + '_all'): (t['description'] + ' all projects') ) {
        projects.each { 
            if(!(t['exclude'] && it.contains('_'))){
                depends(t['name'] + '_' + it) 
            }
        }
    }
}
