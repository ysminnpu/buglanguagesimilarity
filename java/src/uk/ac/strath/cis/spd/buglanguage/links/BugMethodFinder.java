package uk.ac.strath.cis.spd.buglanguage.links;

/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies 2012
 */

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import uk.ac.strath.cis.spd.buglanguage.links.DiffInputListener.Diff;
import uk.ac.strath.cis.spd.buglanguage.links.LogInputListener.Revision;
import uk.ac.strath.cis.spd.buglanguage.links.git.GitDiffInputListener;
import uk.ac.strath.cis.spd.buglanguage.links.git.GitLogInputListener;
import uk.ac.strath.cis.spd.buglanguage.links.svn.SvnDiffInputListener;
import uk.ac.strath.cis.spd.buglanguage.links.svn.SvnLogInputListener;
import uk.ac.strath.cis.spd.buglanguage.parse.JavaParser;
import uk.ac.strath.cis.spd.buglanguage.parse.MethodResult;

public class BugMethodFinder
{
	private static final InputListener PRINT = new PrintInputListener();
	
	private JavaParser parser;
        private File workingCopy;
        private List<String> auth;
        private String scmType;

        private void setParser(JavaParser parser){
            this.parser = parser;
        }

        private void setScmType(String scmType){
            this.scmType = scmType;
        }

        private void setWorkingCopy(File workingCopy){
            this.workingCopy = workingCopy;
        }

        private void setAuth(List<String> auth){
            this.auth = auth;
        }

	private List<String> execute(File workingDirectory, InputListener listener, String... command) throws IOException
	{
		try
		{
			ProcessBuilder builder = new ProcessBuilder(command);
			builder.directory(workingDirectory);
			Process process = builder.start();

			List<String> lines = new ArrayList<String>();
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			while ((line = br.readLine()) != null)
			{
				if (listener != null)
				{
					listener.processLine(line);
				}
			}

			int result = process.waitFor();
			if (result != 0)
			{
				throw new IOException("Unexpected result " + result + " checking out project. Error was " + IOUtils.toString(process.getErrorStream()));
			}
			return lines;
		}
		catch (IOException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new IOException(e);
		}
	}

        private void scm(String scm, InputListener listener, String... args) throws IOException
        {
            List<String> command = new ArrayList<String>();
            command.add(scm);
            if(auth != null)
            {
                command.addAll(auth);
            }
            command.addAll(Arrays.asList(args));
            execute(    workingCopy,
                        listener,
                        command.toArray(new String[0]));
        }

	private void update() throws IOException
	{
            if(scmType.equals("git")){
                scm("git", PRINT, "checkout", "HEAD");
            }
            else{
                scm("svn", PRINT, "up");
            }
	}

	private void changeToRevision(String revision) throws IOException
	{
            if(scmType.equals("git")){
                scm("git", PRINT, "checkout", revision);
            }
            else{
                scm("svn", PRINT, "up", "-r", revision);
            }
	}

	private List<Revision> parseLog(String bugPattern, String startRevision, String endRevision) throws IOException
	{
            LogInputListener listener;
            if(scmType.equals("git")){
                listener = new GitLogInputListener(bugPattern);
                String limit = "";
                if (endRevision != null){
                    if(startRevision != null){
                        limit = startRevision + ".." + endRevision;
                    }
                    else{
                        limit = endRevision;
                    }
                }
                scm("git", listener, "log", "--pretty=format:Hash: %H%n%s%n%b", limit);
            }
            else{
                listener = new SvnLogInputListener(bugPattern);
                String limit = "";
                if (endRevision != null){
                    if(startRevision != null){
                        limit = startRevision + ":" + endRevision;
                    }
                    else{
                        limit = "1:" + endRevision;
                    }
                }
                scm("svn", listener, "log", limit);
            }
            return listener.getRevisions();
	}
	
	private List<Diff> diff(String revision) throws IOException{
            DiffInputListener listener = null;
            if(scmType.equals("git")){
                listener = new GitDiffInputListener();
                scm("git", listener, "diff", revision);
            }
            else{
                listener = new SvnDiffInputListener();
                scm("svn", listener, "diff", "-r", revision, "--diff-cmd", "diff", "-x", "-bU0");
            }
            return listener.getChanges();
	}
	
	private List<String> getChangedMethods(List<Diff> changes) throws FileNotFoundException, IOException {
		String lastFile = null;
		MethodResult[] parsedMethods = new MethodResult[0];
		int i = 0;
		List<String> methodNames = new ArrayList<String>();
		for (Diff change : changes)
		{
			String filename = change.getFile();
                        if(scmType.equals("git")){
                            filename = filename.replaceAll("^[ab]/", "");
                        }
			int start = change.getFrom(false);
			int end = change.getTo(false);
			System.out.println(filename + ": " + start + " - " + end);
			if(start == 0 || end == 0) {
				continue;
			}
			
			if(!filename.equals(lastFile)) {
				lastFile = filename;
				File file = new File(workingCopy, filename);
                                if(parser.shouldParseFile(workingCopy, file)){
                                    parsedMethods = parser.extractMethodsForFile(file);
                                    i = 0;
                                }
			}
			
			while(i<parsedMethods.length) {
				if(end < parsedMethods[i].getStartLine()) {
					break;
				}
				
				if(start < parsedMethods[i].getEndLine()) {
					methodNames.add(parsedMethods[i].getName());
				}
				i++;
			}	
		}
		return methodNames;
	}
	
	private Map<String, Set<String>> generateDiffs(List<Revision> revisions) throws IOException {
		Map<String, Set<String>> methodsByBug = new HashMap<String, Set<String>>();
		for (Revision revision : revisions)
		{
			String revisionNumber = revision.getRevision();
			
                        try
                        {
                            changeToRevision(revisionNumber);
                            
                            String previous;
                            if(scmType.equals("git")){
                                previous = revisionNumber + "^";
                            }
                            else{
                                previous = String.valueOf(Integer.parseInt(revisionNumber) - 1);
                            }
                            List<Diff> changes = diff(previous);
                            List<String> methodNames = getChangedMethods(changes);
                            
                            List<String> bugs = revision.getBugs();
                            for (String bug : bugs)
                            {
                                    Set<String> methodsForBug = methodsByBug.get(bug);
                                    if(methodsForBug == null) {
                                            methodsForBug = new HashSet<String>();
                                            methodsByBug.put(bug, methodsForBug);
                                    }
                                    
                                    methodsForBug.addAll(methodNames);
                            }
                        }
                        catch(IOException e)
                        {
                            System.out.println("Exception processing revision " + revisionNumber);
                            e.printStackTrace();
                            break;
                        }
		}
		
		return methodsByBug;
	}
	
	private void writeBugsToFile(File outputDir, Map<String, Set<String>> methodsByBug) throws FileNotFoundException {
		for (String bug : methodsByBug.keySet())
		{
			List<String> methodsToWrite = new ArrayList<String>(methodsByBug.get(bug));
			if(!methodsToWrite.isEmpty()) {
				Collections.sort(methodsToWrite);
				
				File file = new File(outputDir, "GoldSet" + bug + ".txt");
				PrintWriter writer = null;
				try
				{
					FileOutputStream outputStream = new FileOutputStream(file, true);
					writer = new PrintWriter(new BufferedOutputStream(outputStream));
					for (String methodName : methodsToWrite)
					{
						writer.println(methodName);
					}
				}
				finally
				{
					IOUtils.closeQuietly(writer);
				}
			}
		}
	}

        public void writeBugs(File output, String bugPattern, String startRevision, String endRevision) throws IOException, ClassNotFoundException
        {
                output.mkdir();
                FileUtils.cleanDirectory(output);
                List<Revision> revisions = parseLog(bugPattern, startRevision, endRevision);
                List<Revision> filtered = new ArrayList<Revision>();
                for (Revision rev : revisions)
                {
                        if(process(rev)) {
                                filtered.add(rev);
                        }
                }
                Map<String, Set<String>> methodsByBug = generateDiffs(filtered);
                writeBugsToFile(output, methodsByBug);
        }

	public static void main(String[] args) throws IOException, ClassNotFoundException
	{
		Properties config = new Properties();
		config.load(new FileReader(args[0]));

		BugMethodFinder finder = new BugMethodFinder();

		File workingCopy = new File(config.getProperty("working_copy"));
		File output = new File(config.getProperty("old_methods"));
		String bugPattern = config.getProperty("bug_pattern");
		String auth = config.getProperty("svn_auth");
                String startRevision = config.getProperty("start_revision");
                String endRevision = config.getProperty("end_revision");
                String scmType = config.getProperty("repository_type", "svn");
                boolean partition = Boolean.parseBoolean(config.getProperty("partition"));
		File partionedOutput = new File(config.getProperty("methods"));

                File methodIds = new File(config.getProperty("method_ids"));
                String patterns = config.getProperty("file_patterns");
                JavaParser parser = new JavaParser(methodIds, patterns);
                finder.setParser(parser);

                finder.setWorkingCopy(workingCopy);
                if(auth != null){
                    finder.setAuth(Arrays.asList(auth.split(" ")));
                }
                finder.setScmType(scmType);

		finder.changeToRevision(endRevision);
                try
                {
                    if(partition)
                    {
                        finder.writeBugs(output, bugPattern, null, startRevision);
                        finder.writeBugs(partionedOutput, bugPattern, startRevision, endRevision);
                    }
                    else
                    {
                        finder.writeBugs(output, bugPattern, null, null);
                    }
                }
                finally
                {
                    finder.changeToRevision(startRevision);
                }
	}
	
	private static boolean process(Revision revision) {
            return true;
	}

}
