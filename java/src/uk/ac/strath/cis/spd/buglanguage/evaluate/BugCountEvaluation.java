package uk.ac.strath.cis.spd.buglanguage.evaluate;

/*
 * See LICENCE_BSD for licensing information.
 *
 * Copyright Steven Davies 2012
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils; 

public class BugCountEvaluation extends Evaluation
{
        private List<Long> allBugs;

	public BugCountEvaluation(String config) throws Exception
	{
            super(config, "bug_count");
            allBugs = new ArrayList<Long>();
	}
	
        @Override
        protected void filterBug(File bug, long bugId) throws Exception
        {
            allBugs.add(bugId);
        }

        public void createInstanceStructure() throws Exception
        {
            filterBugs(bugs, true);
            filterBugs(oldBugs, false);
            Collections.sort(allBugs);
        }

	protected void evaluate(String methodId, String method) throws Exception
	{
            int count = 0;
            int numBugs = 0;
            
            for(long bugId: allBugs)
            {
                    boolean expected = bugsByMethod.get(method).contains(bugId);
                    
                    if(bugsToEvaluate.contains(bugId)) {
                            print(bugId, methodId, count > 0 ? 1 : 0);
                    }
                    
                    if(expected) {
                            count++;
                    }
                    numBugs++;
            }
	}

	public static void main(String[] argv) throws Exception
	{
                BugCountEvaluation evaluation = new BugCountEvaluation(argv[0]);
                try
                {
                    evaluation.processLinks();
                    evaluation.createInstanceStructure();
                    evaluation.evaluate();
                }
                finally
                {
                    evaluation.cleanup();
                }
	}

}
