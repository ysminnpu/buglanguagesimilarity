/**
 * See LICENCE_BSD for licensing information
 *
 * Coyright Steven Davies 2012
 */
package uk.ac.strath.cis.spd.buglanguage.links.git;

import uk.ac.strath.cis.spd.buglanguage.links.DiffInputListener;

import java.util.regex.Pattern;

public class GitDiffInputListener extends DiffInputListener
{
	private static final Pattern filePattern = Pattern.compile("^\\+\\+\\+\\s+(.+)(?:\\s+\\(.+\\))?$");
	private static final Pattern hunkPattern = Pattern.compile("^@@\\s+-(\\d+)(?:,(\\d+))?\\s+\\+(\\d+)(?:,(\\d+))?\\s+@@.*$");

        @Override
        protected Pattern getFilePattern() { 
            return filePattern; 
        }

        @Override
        protected Pattern getHunkPattern() { 
            return hunkPattern; 
        }
}
