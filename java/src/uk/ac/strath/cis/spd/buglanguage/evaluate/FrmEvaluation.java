package uk.ac.strath.cis.spd.buglanguage.evaluate;

/*
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public
 *   License
 *   along with this program.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

/*
 * FrmEvaluation.java
 * Copyright Steven Davies 2012     
 */

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils; 

import weka.classifiers.Classifier;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.PolyKernel;
import weka.classifiers.trees.J48;
import weka.core.converters.DatabaseLoader;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.SparseInstance;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToBinary;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.filters.unsupervised.instance.NonSparseToSparse;

public class FrmEvaluation extends Evaluation
{
    private File classifiers;
    private Attribute id, summary, relevant;
    private Instances allBugs;
    private long seed;
    private Random random;
    private FastVector attributes;
    private Instances inputFormat;
    private StringToWordVector filter;

    public FrmEvaluation(String config) throws Exception
    {
        super(config, "similarity");
        Properties props = new Properties();
        props.load(new FileReader(config));
        classifiers = new File(props.getProperty("classifiers"));
        seed = Long.parseLong(props.getProperty("seed"));
        random = new Random(seed);
        classifiers.mkdir();
        FileUtils.cleanDirectory(classifiers);
    }
    
    @Override
    protected void filterBug(File bug, long bugId) throws Exception
    {
        Instance instance = new Instance(attributes.size());
        instance.setValue(id, bugId);
        instance.setValue(summary, FileUtils.readFileToString(bug));
        instance.setValue(relevant, String.valueOf(false));
        instance.setDataset(inputFormat);
        filter.input(instance);
    }

    public void createInstanceStructure() throws Exception
    {
        FastVector values = new FastVector();
        values.addElement(String.valueOf(true));
        values.addElement(String.valueOf(false));

        id = new Attribute("id");
        summary = new Attribute("summary", (FastVector) null);
        relevant = new Attribute("relevant", values);

        attributes = new FastVector();
        attributes.addElement(id);
        attributes.addElement(summary);
        attributes.addElement(relevant);

        inputFormat = new Instances("bug", attributes, 0);
                                                                                                                                
        filter = new StringToWordVector();
        filter.setInputFormat(inputFormat);
        filter.setLowerCaseTokens(true);
        filter.setTokenizer(new AlphabeticIdentifierTokenizer());
        filter.setUseStoplist(true);
        filter.setIDFTransform(true);
        filter.setWordsToKeep(10000);

        filterBugs(bugs, true);
        filterBugs(oldBugs, false);

        filter.batchFinished();

        allBugs = new Instances(filter.getOutputFormat());
        Instance filtered;
        while((filtered = filter.output()) != null)
        {
            allBugs.add(filtered);
        }
        allBugs.sort(id);
    }

    protected void evaluate(String methodId, String method) throws Exception
    {
        Instances tempSet = new Instances(allBugs, allBugs.numInstances());
        tempSet.deleteAttributeAt(0);
        tempSet.setClassIndex(0);
        
        Instances classifySet = new Instances(allBugs, allBugs.numInstances());
        classifySet.deleteAttributeAt(0);
        classifySet.setClassIndex(0);
        
        J48 classifier = new J48();
        classifier.setMinNumObj(1);
        classifier.setUnpruned(true);
        
        boolean rebuild = false;
        for(int i=0; i<allBugs.numInstances(); i++)
        {
            SparseInstance source = (SparseInstance) allBugs.instance(i);
            
            long bugId = (long) source.value(0);
            boolean expected = bugsByMethod.get(method).contains(bugId);
            
            if(bugsToEvaluate.contains(bugId)) {
                double probability = 0;
                if(rebuild){
                    classifier.buildClassifier(classifySet);
                    rebuild = false;
                }
                if(classifySet.numInstances()>0){
                    SparseInstance tempInstance = copyInstance(source, tempSet, expected);
                    probability = Boolean.parseBoolean(relevant.value((int)classifier.classifyInstance(tempInstance))) ? 1 : 0;
                }
                print(bugId, methodId, probability);
                if(expected && probability>0.75)
                {
                    writeClassifierDetails(classifier, methodId, bugId);
                }
            }
            
            if(expected || random.nextDouble()<=0.05) {
                SparseInstance classifyInstance = copyInstance(source, classifySet, expected);
                classifySet.add(classifyInstance);
                rebuild = true;
            }
        }
    }

    private SparseInstance copyInstance(SparseInstance source, Instances dataSet, boolean expected){
        SparseInstance copy = new SparseInstance(1, new double[source.numAttributes()-1]);
        copy.setDataset(dataSet);
        
        for(int j=0; j<source.numValues(); j++) {
            int index = source.index(j);
            if (index > 0)
            {
                copy.setValue(index - 1, source.valueSparse(j));
            }
        }
        copy.setClassValue(String.valueOf(expected));
        return copy;
    }

    private void writeClassifierDetails(Classifier classifier, String method, long bugId) throws Exception
    {
        PrintWriter details = new PrintWriter(new BufferedOutputStream(FileUtils.openOutputStream(new File(classifiers, bugId + "_" + method))), true);
        details.write(classifier.toString());
        details.close();
    }

    protected void writeMetaData(long bugId) throws Exception
    {
        super.writeMetaData(bugId);
        print(bugId, "#", "Seed", seed);
    }

    public static void main(String[] argv) throws Exception
    {
        FrmEvaluation evaluation = new FrmEvaluation(argv[0]);
        try
        {
            evaluation.processLinks();
            evaluation.createInstanceStructure();
            evaluation.evaluate();
        }
        finally
        {
            evaluation.cleanup();
        }
    }

}
