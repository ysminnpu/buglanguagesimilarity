'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

import logging
from os import path, listdir, mkdir, sep as path_sep
from shutil import rmtree
import errno
from Utils import get_config
from VersionUtils import write_metadata
from CorpusUtils import CorpusUtils
from gensim.models import TfidfModel, LdaModel, LsiModel
from gensim.parsing.preprocessing import preprocess_string
import sys

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def get_bow(dictionary, bug):
    return dictionary.doc2bow(preprocess_string(bug.read()))

def create_files(config, models, bug_id):
    out_files = {}
    for key in models.keys():
        out_file = open(path.join(config['results'], key, bug_id), 'w', 1)
        write_metadata(out_file, __file__, None, ['data/'])
        out_files[key] = out_file
    
    return out_files

def evaluate_bugs(bugs, config, models, dictionary):
    for f in listdir(bugs):
        if f.startswith('LongDescription'):
            bug_id = f.replace('LongDescription', '').replace('.txt', '')
            out_files = create_files(config, models, bug_id)

            with open(path.join(bugs, f)) as bug:
                bow = get_bow(dictionary, bug)
                for key, (model, index) in models.items():
                    query = model[bow] if model else bow
                    for document, similarity in enumerate(index[query]):
                        out_files[key].write('\t'.join((str(document), str(similarity))))
                        out_files[key].write('\n')
            for out_file in out_files.values():
                out_file.close()

def evaluate():
    config = get_config(sys.argv[1], 'config')
    corpus_utils = CorpusUtils(config)
    corpus_utils.load_all()
    
    corpus, dictionary = corpus_utils.get_corpus_and_dictionary(config)
    
    models = {}
    
    key = 'Tfidf'
    model = corpus_utils.get_model(TfidfModel, key)
    sim = corpus_utils.get_index(key)
    models[key] = (model, sim)
    
    for num_topics in [50,100,200,300,500]:
        for prefix, model_class in [('Lda', LdaModel), ('Lsi', LsiModel)]:
            key = prefix + ' ' + str(num_topics)
            model = corpus_utils.get_model(model_class, key)
            sim = corpus_utils.get_index(key)
            models[key] = (model, sim)
    for key in models.keys():
        results = path.join(config['results'], key)
        rmtree(results, ignore_errors=True)
        try:
            mkdir(results)
        except OSError as e:
            if e.errno == errno.EEXIST: pass
            else: raise

    evaluate_bugs(config['bugs'], config, models, dictionary)

if __name__ == '__main__':
    evaluate()
