'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from operator import itemgetter
from os import path, listdir, mkdir
from shutil import rmtree
import sys
import errno
from Utils import get_config
from VersionUtils import write_metadata

def count_predictions(bug_id, out_file, results):
    count = 0
    with open(path.join(results, 'similarity', bug_id)) as f:
        for line in f:
            if line.startswith('#'): continue
            method_id, score = line.split()
            if score == '1.0':
                count += 1

    out_file.write('\t'.join((bug_id, str(count))))
    out_file.write('\n')

if __name__ == '__main__':
    config = get_config(sys.argv[1], 'config')
    results = config['results']

    with open(path.join(results, 'similarity.pred'), 'w', 1) as out_file: 
        write_metadata(out_file, __file__)
        for bug_id in listdir(path.join(results, 'relevant')):
            count_predictions(bug_id, out_file, results)

