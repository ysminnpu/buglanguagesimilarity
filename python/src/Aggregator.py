'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from Utils import get_config
import errno
from os import listdir, mkdir, path
from shutil import rmtree
import sys
    
def strip(line):
    parts = line.split('.')
    line = ''
    for part in parts[:-1]:
        line = line + '.' + part
        if part[0].isupper():
            break
    return line[1:]

def get_dirs(config, key):
    try:
        src = config['src_' + key]
    except KeyError as e:
        print 'No source dir specified. Exiting.'
        quit()
    dst = config[key]
    rmtree(dst, True)
    mkdir(dst)
    return src, dst

def copy_files(src, dst):
    for f in listdir(src):
        trimmed = set()
        with open(path.join(src, f)) as i:
            for line in i:
                trimmed.add(strip(line))
        with open(path.join(dst, f), 'w') as o:
            o.writelines('\n'.join(trimmed))

if __name__ == '__main__':
    config = get_config(sys.argv[1], 'config')

    src,dst = get_dirs(config, 'methods')
    copy_files(src, dst)

    src,dst = get_dirs(config, 'old_methods')
    copy_files(src, dst)

    src,dst = get_dirs(config, 'corpus')
    method_ids = []
    with open(config['src_method_ids']) as f:
        for line in f:
            method_id, method = line.strip().split()
            method_ids += [method]
    class_ids = {}
    with open(config['method_ids'], 'w') as id_file:
        for f in listdir(src):
            with open(path.join(src, f)) as i:
                method = method_ids[int(f)]
                parent = strip(method)
                try:
                    parent_id = class_ids[parent]
                except KeyError:
                    parent_id = str(len(class_ids))
                    class_ids[parent] = parent_id
                    id_file.write('\t'.join((parent_id, parent)) + '\n')
                with open(path.join(dst, parent_id), 'a') as o:
                    o.writelines('\n'.join(i.readlines()))

