'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from gensim.corpora import MmCorpus, Dictionary
from gensim.utils import pickle, unpickle
from os import listdir, path, mkdir
import errno
import re
from mimetypes import guess_type 
from gensim.similarities.docsim import Similarity
from gensim.parsing.preprocessing import preprocess_string

def pairs(iterable):
    for i in range(len(iterable)):
        if i == 0:
            yield (None, iterable[i])
        else:
            yield (iterable[i-1], iterable[i])
    yield (iterable[i], None)

def extract_word(word):
    return preprocess_string(word)

def extract_words(line):
    words = []
    for word in re.split('[^A-Za-z]+', line):
        if word:
            split = ''
            for last, current in pairs(word):
                if last is None:
                    split += current
                elif current is None:
                    words += extract_word(split)
                elif current.isupper() and last.islower():
                    words += extract_word(split)
                    split = current
                else:
                    split += current
    return words

class FolderCorpus(object):
    def __init__(self, root):
        self.root = root
    
    def __iter__(self):
        for filename in sorted(listdir(self.root), key=int):
            words = []
            file_path = path.join(self.root, filename)
            with open(file_path) as f:
                for line in f:
                    words += extract_words(line)
                yield words

class BowCorpus(object):
    def __init__(self, dictionary, corpus):
        self.dictionary = dictionary
        self.corpus = corpus
        
    def __iter__(self):
        for doc in self.corpus:
            yield self.dictionary.doc2bow(doc)
            
    def __len__(self):
        return len(self.corpus)
    
class CorpusUtils(object):
    
    def __init__(self, corpus_data):
        self.corpus_data = corpus_data
        self.load = {}
        self.load['corpus'] = False
        self.load['Tfidf'] = False
        for num_topics in [50,100,200,300,500]:
            for prefix in ['Lda', 'Lsi']:
                self.load[prefix + ' ' + str(num_topics)] = False
        for key in self.load.keys():
            self.load[key + '.index'] = False
        self.load['dictionary'] = False
	try:
		mkdir(self.corpus_data['index'])
	except OSError as e:
		if e.errno == errno.EEXIST: pass
		else: raise
	self.corpus_loc = path.join(self.corpus_data['index'], 'corpus.mm')
	self.dict_loc = path.join(self.corpus_data['index'], 'corpus.dict')
    
    def load_all(self):
        for key in self.load.keys():
            self.load[key] = True
        
    def recreate_all(self):
        for key in self.load.keys():
            self.load[key] = False
        
    def recreate_indices(self):
        for key in self.load.keys():
            if key.endswith('.index'):
                self.load[key] = False
    
    def recreate_model(self, prefix):
        for key in self.load.keys():
            if key.startswith(prefix):
                self.load[key] = False

    def get_corpus_and_dictionary(self, config):    
        dictionary = None
        if self.load['corpus']:
            corpus = MmCorpus(self.corpus_loc)
            if self.load['dictionary']:
                dictionary = Dictionary.load(self.dict_loc)
        else:
            corpus = FolderCorpus(self.corpus_data['corpus'])
        
        if not dictionary:
            dictionary = Dictionary()
            for document in corpus:
                dictionary.add_documents([document])
            if not self.load['corpus']:
                corpus = BowCorpus(dictionary, corpus)
        return corpus, dictionary
    
    def get_model(self, model=None, key=None, *args, **kwargs):
        if self.load[key]:
            return model.load(path.join(self.corpus_data['index'], key + '.model'))
        
        return model(*args, **kwargs)
    
    def get_index(self, key, *args, **kwargs):
	index_loc = path.join(self.corpus_data['index'], key + '.index')
        if self.load[key + '.index']:
            return Similarity.load(index_loc)
        
        return Similarity(index_loc, *args, **kwargs)
    
    def save_corpus(self, corpus, dictionary):
        if not self.load['corpus']:
            MmCorpus.serialize(self.corpus_loc, corpus, id2word=dictionary)
    
    def save_dictionary(self, dictionary):
        if not self.load['dictionary']:
            dictionary.save(self.dict_loc)
       
    def save_model(self, model, key):
        if not self.load[key]:
            model.save(path.join(self.corpus_data['index'], key + '.model'))
            
    def save_index(self, index, key):
        if not self.load[key + '.index']:
            index.save()
